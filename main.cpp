
#ifndef __PROGTEST__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;

class InvalidRangeException {

};

#endif /* __PROGTEST__ */

// uncomment if your code implements initializer lists
#define EXTENDED_SYNTAX

//function stores ranges lo,hi
class CRange {
public:
    CRange(long long lo, long long hi) {
        if (lo > hi) {
            throw InvalidRangeException();
        }
        _lo = lo;
        _hi = hi;
    }

    long long int getLo() const {
        return _lo;
    }

    long long int getHi() const {
        return _hi;
    }

    template<class _T>
    //function returns correctly formatted output
    friend _T &operator<<(_T &stream, const CRange &x) {
        //saving the original base
        ios_base::fmtflags storedFlags(stream.flags());
        //changing to base 10
        stream << dec;
        //formatting output
        if (x.getLo() == x.getHi()) {
            stream << x.getLo();
        } else {
            stream << "<" << x.getLo() << ".." << x.getHi() << ">";
        }
        //returning original base
        stream.flags(storedFlags);
        return stream;
    }


private:
    //lower bound of range
    long long _lo;
    //upper bound of range
    long long _hi;
};

//comparison function
bool comp(CRange left, long long right) {

    return left.getHi() < right;
}


class CRangeList {
private:

    //container for storing ranges
    vector<CRange> _ranges;

public:
    CRangeList() = default;

    template<class T>
    //constructor for list of ranges

    CRangeList(initializer_list<initializer_list<T>> ranges) {
        for (auto range : ranges) {
            if (range.size() == 2) {
                *this += CRange(*range.begin(), *(range.begin() + 1));
            } else {
                throw InvalidRangeException();
            }

        }
    }

    //function returns begining iterator from container _ranges
    vector<CRange>::iterator begin() {
        return _ranges.begin();
    }

    //function returns end iterator from container _ranges
    vector<CRange>::iterator end() {
        return _ranges.end();
    }

    //function returns correctly formatted output
    template<class _T>
    friend _T &operator<<(_T &stream, const CRangeList &x) {
        //saving the original base
        ios_base::fmtflags storedFlags(stream.flags());
        //changing to base 10
        stream << dec;
        stream << "{";
        //formatting output
        for (unsigned int i = 0; i < x._ranges.size(); i++) {
            if (x._ranges[i].getLo() == x._ranges[i].getHi()) {
                stream << x._ranges[i].getLo();
            } else {
                stream << "<" << x._ranges[i].getLo() << ".." << x._ranges[i].getHi() << ">";
            }
            if (x._ranges.size() - 1 != i) {
                stream << ",";
            }
        }
        //returning original base
        stream.flags(storedFlags);
        stream << "}";
        return stream;
    }


    CRangeList &operator+=(CRange newRange) {
        /*
         * if newRange is empty returns unchanged container _ranges
         *
         * function figuresout where to put the new range
         * if new range coincides with existing range it updates the range to make it as big as the existing range
         * then it deletes all the ranges that it replaces
         * and finally it saves the new range
         *
         */

        if (_ranges.empty()) {
            *this = newRange;
            return *this;
        }

        unsigned long long whereLo = where(newRange.getLo());
        unsigned long long whereHi = where(newRange.getHi());
        long long loNum, hiNum;
        bool isLow = false, isHigh = false;

        //where should lowerbound go
        if (whereLo != 0) {
            if (_ranges[whereLo - 1].getHi() + 1 == newRange.getLo()) {
                whereLo--;
                isLow = true;
            }
        }
        //where should upperbound go
        if (whereHi != _ranges.size()) {
            if (_ranges[whereHi].getLo() - 1 == newRange.getHi()) {

                isHigh = true;
            }
        }
        //does lowerbound belong to an already existing range
        if (Includes(newRange.getLo(), whereLo) || isLow) {

            loNum = _ranges[whereLo].getLo();
            if (loNum > newRange.getLo()) {
                loNum = newRange.getLo();
            }
            //updates newRange to make it as big as lowerbound of an already existing range
            newRange = CRange(loNum, newRange.getHi());
        }
        //does upperbound belong to an already existing range
        if (Includes(newRange.getHi(), whereHi) || isHigh) {

            hiNum = _ranges[whereHi].getHi();
            if (hiNum < newRange.getHi()) {
                hiNum = newRange.getHi();
            }
            whereHi++;
            //updates newRange to make it as big as upperbound of an already existing range
            newRange = CRange(newRange.getLo(), hiNum);
        }
        //deletes all the ranges the newRange replaces
        _ranges.erase(_ranges.begin() + whereLo, _ranges.begin() + whereHi);
        //inserts newRange
        _ranges.insert(_ranges.begin() + whereLo, newRange);
        return *this;
    }

    CRangeList &operator+=(const CRangeList &newRangeList) {

        CRangeList temp = newRangeList;
        for (auto &range:temp._ranges) {
            *this += range;
        }
        return *this;
    }

    CRangeList operator+(const CRange &newRange) const {
        CRangeList newList = *this;
        return newList += newRange;
    }

    CRangeList operator+(const CRangeList &newRangeList) const {

        CRangeList newList = *this;
        return newList += newRangeList;
    }

    CRangeList &operator-=(CRange deleteRange) {
        /*
        * if newRange is empty returns unchanged container _ranges
        *
        *
        * function determines to what ranges does the deleteRange belong to and if it should split some ranges then it
        * saves the lower and upper bound of the already existing ranges
        *
        * every range that belongs to deleteRange will be deleted
        *
        * then we will insert updated ranges that should have been deleted only partially
        *
        */
        if (_ranges.empty()) {
            return *this;
        }
        long long whereLo = where(deleteRange.getLo());
        long long whereHi = where(deleteRange.getHi());
        //the numbers are used to recreate the deleted ranges that should have been deleted only partially
        long long loNumlo = 0, hiNumHi = 0;
        //where should upperbound go

        bool wasHigh = false, wasLow = false;
        if (Includes(deleteRange.getHi(), whereHi)) {
            hiNumHi = _ranges[whereHi].getHi();
            whereHi++;
            wasHigh = true;
        }
        //where should lowerbound go
        if (Includes(deleteRange.getLo(), whereLo)) {
            loNumlo = _ranges[whereLo].getLo();
            wasLow = true;
        }
        //delete all the ranges the deleteRange intersects
        _ranges.erase(_ranges.begin() + whereLo, _ranges.begin() + whereHi);
        //repair the ranges that should have been deleted only partially
        if (wasHigh) {
            if (hiNumHi != deleteRange.getHi()) {
                *this += CRange(deleteRange.getHi() + 1, hiNumHi);
            }
        }
        if (wasLow) {
            if (loNumlo != deleteRange.getLo()) {
                *this += CRange(loNumlo, deleteRange.getLo() - 1);
            }
        }

        return *this;
    }

    CRangeList &operator-=(const CRangeList &deleteRangeList) {
        for (auto &range:deleteRangeList._ranges) {
            *this -= range;
        }
        return *this;
    }

    CRangeList operator-(const CRange &deleteRange) const {
        CRangeList newList = *this;
        return newList -= deleteRange;
    }

    CRangeList operator-(const CRangeList &delRangeList) const {
        CRangeList newList = *this;
        return newList -= delRangeList;
    }

    //function deletes the whole container _ranges and adds the a new range
    CRangeList &operator=(CRange onlyRange) {

        _ranges.clear();
        _ranges.push_back(onlyRange);
        return *this;
    }

    //function compares all the ranges and returns true if ranges are identical
    bool operator==(const CRangeList &testList) const {
        if (_ranges.size() != testList._ranges.size()) {
            return false;
        }
        for (unsigned int i = 0; i < _ranges.size(); i++) {
            if (_ranges[i].getLo() != testList._ranges[i].getLo() ||
                _ranges[i].getHi() != testList._ranges[i].getHi()) {
                return false;
            }
        }
        return true;
    }

    bool operator!=(const CRangeList &testList) const {
        return !(*this == testList);
    }

    bool Includes(long long number) const {
        long long whereToLook = where(number);
        return Includes(number, whereToLook);

    }

    //function returns true if a specific range exists in container _ranges
    bool Includes(const CRange &range) const {
        long long whereLo = where(range.getLo());
        long long whereHi = where(range.getHi());

        return whereHi == whereLo && Includes(range.getHi(), whereHi) && Includes(range.getLo(), whereLo);

    }

private:
    //function returns true if a specific number exists in one of the ranges in container _ranges
    bool Includes(long long number, unsigned long long whereToLook) const {
        if (whereToLook == _ranges.size()) {
            return false;
        }
        return _ranges[whereToLook].getLo() <= number && _ranges[whereToLook].getHi() >= number;

    }

    //function returns where a number should be placed in container _ranges
    long long where(long long number) const {
        return lower_bound(_ranges.begin(), _ranges.end(), number, comp) - _ranges.begin();
    }
};


//function creates a new rangelist and connects two ranges together and returns the new rangelist
CRangeList operator+(const CRange &newRange, const CRange &newRange2) {
    CRangeList newList;
    newList = newRange;
    newList += newRange2;
    return newList;
}
//function creates a new rangelist and from one range subtracts the second one and returns the new rangelist

CRangeList operator-(const CRange &newRange, const CRange &deleteRange) {
    CRangeList newList;
    newList = newRange;
    newList -= deleteRange;
    return newList;
}

#ifndef __PROGTEST__

string toString(const CRangeList &x) {
    ostringstream oss;
    oss << x;
    return oss.str();
}

int main(void) {


    CRangeList a, b;

    assert (sizeof(CRange) <= 2 * sizeof(long long));
    a = CRange(5, 10);
    a += CRange(25, 100);
    assert (toString(a) == "{<5..10>,<25..100>}");
    a += CRange(-5, 0);
    a += CRange(8, 50);
    assert (toString(a) == "{<-5..0>,<5..100>}");
    a += CRange(101, 105) + CRange(120, 150) + CRange(160, 180) + CRange(190, 210);
    assert (toString(a) == "{<-5..0>,<5..105>,<120..150>,<160..180>,<190..210>}");
    a += CRange(106, 119) + CRange(152, 158);
    assert (toString(a) == "{<-5..0>,<5..150>,<152..158>,<160..180>,<190..210>}");
    a += CRange(-3, 170);
    a += CRange(-30, 1000);
    assert (toString(a) == "{<-30..1000>}");
    b = CRange(-500, -300) + CRange(2000, 3000) + CRange(700, 1001);
    a += b;
    assert (toString(a) == "{<-500..-300>,<-30..1001>,<2000..3000>}");
    a -= CRange(-400, -400);
    assert (toString(a) == "{<-500..-401>,<-399..-300>,<-30..1001>,<2000..3000>}");
    a -= CRange(10, 20) + CRange(900, 2500) + CRange(30, 40) + CRange(10000, 20000);
    assert (toString(a) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    try {
        a += CRange(15, 18) + CRange(10, 0) + CRange(35, 38);
        assert ("Exception not thrown" == NULL);
    }
    catch (const InvalidRangeException &e) {
    }
    catch (...) {
        assert ("Invalid exception thrown" == NULL);
    }
    assert (toString(a) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    b = a;
    assert (a == b);
    assert (!(a != b));
    b += CRange(2600, 2700);
    assert (toString(b) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}");
    assert (a == b);
    assert (!(a != b));
    b += CRange(15, 15);
    assert (toString(b) == "{<-500..-401>,<-399..-300>,<-30..9>,15,<21..29>,<41..899>,<2501..3000>}");
    assert (!(a == b));
    assert (a != b);
    assert (b.Includes(15));
    assert (b.Includes(2900));
    assert (b.Includes(CRange(15, 15)));
    assert (b.Includes(CRange(-350, -350)));
    assert (b.Includes(CRange(100, 200)));
    assert (!b.Includes(CRange(800, 900)));
    assert (!b.Includes(CRange(-1000, -450)));
    assert (!b.Includes(CRange(0, 500)));
    a += CRange(-10000, 10000) + CRange(10000000, 1000000000);
    assert (toString(a) == "{<-10000..10000>,<10000000..1000000000>}");
    b += a;
    assert (toString(b) == "{<-10000..10000>,<10000000..1000000000>}");
    b -= a;
    assert (toString(b) == "{}");
    b += CRange(0, 100) + CRange(200, 300) - CRange(150, 250) + CRange(160, 180) - CRange(170, 170);
    assert (toString(b) == "{<0..100>,<160..169>,<171..180>,<251..300>}");
    b -= CRange(10, 90) - CRange(20, 30) - CRange(40, 50) - CRange(60, 90) + CRange(70, 80);
    assert (toString(b) == "{<0..9>,<20..30>,<40..50>,<60..69>,<81..100>,<160..169>,<171..180>,<251..300>}");
#ifdef EXTENDED_SYNTAX
    CRangeList x{{5,   20},
                 {150, 200},
                 {-9,  12},
                 {48,  93}};
    //cout << x << endl << endl;

    assert (toString(x) == "{<-9..20>,<48..93>,<150..200>}");
    ostringstream oss;
    oss << setfill('=') << hex << left;
    for (const auto &v : x + CRange(-100, -100)) {
        oss << v << endl;

    }

    oss << setw(10) << 1024;
    //cout << oss.str();
    assert (oss.str() == "-100\n<-9..20>\n<48..93>\n<150..200>\n400=======");
#endif /* EXTENDED_SYNTAX */
    return 0;
}


#endif /* __PROGTEST__ */
